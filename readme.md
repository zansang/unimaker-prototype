This project is to carry out the R&D of facial recognition, drowsiness & distraction detection prototypes for drivers.

# Download Links
1. python [v3.6.8-windows-x86-64-executable-installer]: https://www.python.org/downloads/release/python-368/
2. vs-build-tools [build-tools-for-visual-studio-2019]: https://visualstudio.microsoft.com/downloads/

# Installation & Setup Guides
1. Install python and ensure it is registered on system PATH
2. Install C++ Build Tools from Visual Studio 2019 Build Tools
3. Open editor / ide with terminal and change directory to this project root, run command "pip install -r requirements.txt"

# Facial Recognition Execution Guides (run the command from project root folder)
1. Encoding: python facial-recognition\encode_faces.py --dataset facial-recognition\dataset --encodings facial-recognition\encodings.pickle
2. Image Test: python facial-recognition\recognize_faces_image.py --encodings facial-recognition\encodings.pickle --image facial-recognition\sample\image-01.png
3. Video Test: python facial-recognition\recognize_faces_video_file.py --encodings facial-recognition\encodings.pickle --input facial-recognition\sample\video-01.avi
4. Webcam Test: python facial-recognition\recognize_faces_video.py --encodings facial-recognition\encodings.pickle --output facial-recognition\output\webcam-01.avi --display 1

# Inward Detection Execution Guides (run the command from project root folder)
1. Start Program: python .\inward-detection\inward_detection.py
2. Terminate Program: Ctrl + C
