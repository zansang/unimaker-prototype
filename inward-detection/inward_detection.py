from playsound import playsound
from threading import Thread
import drowsy_and_distraction_detection as ddd
import driver_recognition as dr
import datetime as dt
import numpy as np
import imutils
import cv2


def alert_sound():
    playsound('alert.mp3')


# connect to sensor (webcam)
cap = cv2.VideoCapture(0)

time_now = dt.datetime.now()
dd_alert_time = dt.datetime(1900, 1, 1, 0, 0, 0)
dd_alert_duration = 1.5
dd_sound_time = dt.datetime(1900, 1, 1, 0, 0, 0)
dd_sound_duration = 1.45

prev_dd_message = ''

width = cap.get(3)
desire_width = int(width)
r = width / float(desire_width)

while True:
    print((dt.datetime.now() - time_now).total_seconds())
    time_now = dt.datetime.now()
    # capture image frame from sensor (webcam), convert to grayscale and detect frontal face in rectangle within 4 points
    ret, frame = cap.read()

    frame = cv2.flip(frame, 1)
    adj_frame = imutils.resize(frame, width=desire_width)

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    combined_img = np.zeros((*gray.shape, 3), dtype=np.uint8)
    
    (dd_ret, dd_message, dd_img) = ddd.process_image(adj_frame, time_now, combined_img, r)
    (d_ret, d_message, d_img) = dr.process_image(adj_frame, combined_img, r)

    combined_frame = ddd.weighted_img(dd_img, d_img, 1, 1)
    output_frame = ddd.weighted_img(combined_frame, frame, 1, 1)

    if dd_ret != 0:
        prev_dd_message = dd_message
        dd_alert_time = time_now

    if d_ret != 0:
        cv2.putText(output_frame, d_message, (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)

    if (time_now - dd_alert_time).total_seconds() <= dd_alert_duration:
        cv2.putText(output_frame, prev_dd_message, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        if (time_now - dd_sound_time).total_seconds() > dd_sound_duration:
            dd_sound_time = time_now
            t = Thread(target=alert_sound)
            t.deamon = True
            t.start()
    
    # output frame to window
    cv2.imshow("window", output_frame)

    # pending for closing command when "q" key is pressed
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

# close display window
cv2.destroyAllWindows()
cap.release()
