import math
import numpy as np
import pandas as pd
import pandas.io.common
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# import data and calculate number of subplots (row and column) required by using number of columns
data = pd.read_csv('data.csv')
colnames = data.columns
col_size = len(colnames)
col = 3
row = math.ceil(col_size / col)
total = row * col

# select figure style
plt.style.use('fivethirtyeight')

# initiate figure layout
fig, axs = plt.subplots(row, col, figsize=(21, 4), facecolor='w', edgecolor='k')
fig.subplots_adjust(hspace=0.5, wspace=.001)
axs = axs.ravel()

for i in range(total):
    if i >= col_size:
        fig.delaxes(axs[i])

# initiate line color and type
line_type = ['r-', 'b-', 'g-', 'c-', 'm-', 'y-', 'k-']


def animate(i):
    # refresh data as updated information
    try:
        data = pd.read_csv('data.csv')

        # plot each subplot with specific setting and remove unused axes
        for i in range(col_size):
            axs[i].clear()
            axs[i].set_ylabel(colnames[i], fontsize=9)
            axs[i].tick_params(labelsize=9)
            axs[i].grid(linestyle='-', linewidth=0.5)
            axs[i].plot(data[colnames[i]], line_type[i % 7], linewidth=0.5)

        # set figure title and tighten layout
        plt.suptitle("Charts Monitor", fontsize=12)
        plt.tight_layout()
        plt.subplots_adjust(top=0.925)
    except pandas.io.common.EmptyDataError:
        pass

    return True


# animate the chart
ani = animation.FuncAnimation(fig, animate, interval=100)
plt.show()
