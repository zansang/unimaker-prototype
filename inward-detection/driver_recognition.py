# USAGE
# python recognize_faces_video.py --encodings encodings.pickle
# python recognize_faces_video.py --encodings encodings.pickle --output output/jurassic_park_trailer_output.avi --display 0

# import the necessary packages
from imutils.video import VideoStream
import face_recognition
import numpy as np
import imutils
import pickle
import time
import cv2

# distance threshold to determine a matched or not
dist_threshold = 0.4
num_resample = 1

# use distance or majority_vote
compare_method = "distance"
#compare_method = "majority_vote"

# construct the argument parser and parse the arguments
encodings_file = './inward-detection/encodings.pickle'
detection_method = 'hog'

# load the known faces and embeddings
print("[INFO] loading encodings...")
data = pickle.loads(open(encodings_file, "rb").read())

def process_image(frame, combined_img, r):
	global args
	global data

	global dist_threshold
	global num_resample
	global compare_method
	global detection_method

	# convert the input frame from BGR to RGB
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

	# detect the (x, y)-coordinates of the bounding boxes
	# corresponding to each face in the input frame, then compute
	# the facial embeddings for each face
	boxes = face_recognition.face_locations(rgb, model=detection_method)
	encodings = face_recognition.face_encodings(rgb, boxes, num_resample)
	names = []
	match_name_index = {}
	match_name_dist = {}

	# loop over the facial embeddings
	for encoding in encodings:
		# attempt to match each face in the input image to our known
		# encodings
		matches = face_recognition.compare_faces(data["encodings"], encoding,dist_threshold)
		face_dist = face_recognition.face_distance(encoding, data["encodings"])
		#print("dist = {}".format(face_dist))
		name = "Unknown"
		match_name = ""
		match_dist = -1

		# check to see if we have found a match
		if True in matches:
			# find the indexes of all matched faces then initialize a
			# dictionary to count the total number of times each face
			# was matched
			matchedIdxs = [i for (i, b) in enumerate(matches) if b]
			counts = {}

			# loop over the matched indexes and maintain a count for
			# each recognized face face
			# use euclidean distance
			for i in matchedIdxs:
				name = data["names"][i]
				counts[name] = counts.get(name, 0) + 1
				if (match_dist == -1) or (match_dist > face_dist[i]):
					match_name = name
					match_dist = face_dist[i]

			# determine the recognized face with the largest number
			# of votes (note: in the event of an unlikely tie Python
			# will select first entry in the dictionary)
			if compare_method == "distance":
				name_list = [data["names"][i] for i in matchedIdxs]
				dist_list = [face_dist[i] for i in matchedIdxs]
				#print("matches = {}".format(name_list))
				#print("dist = {}".format(dist_list))
				#print("  matched: {}, dist = {}".format(match_name, match_dist))
				name = match_name
			else:
				name = max(counts, key=counts.get)
		
		# update the list of names
		names.append(name)
		if compare_method == "distance":
			if name in match_name_index:
				if match_dist < match_name_dist[name]:
					names[match_name_index[name]] = "Unknown"
					match_name_dist[name] = match_dist
					match_name_index[name] = len(names) - 1
					#print("Re-matched {}, index: {}, dist: {}".format(name, match_name_index[name], match_name_dist[name]))
				else:
					index = len(names) - 1
					names[index] = "Unknown"
			else:
				match_name_dist[name] = match_dist
				match_name_index[name] = len(names) - 1
				#print("Matched {}, index: {}, dist: {}".format(name, match_name_index[name], match_name_dist[name]))

	ret = 0
	message = ''

	# loop over the recognized faces
	for ((top, right, bottom, left), name) in zip(boxes, names):
		# rescale the face coordinates
		top = int(top * r)
		right = int(right * r)
		bottom = int(bottom * r)
		left = int(left * r)
		
		# draw the predicted face name on the image
		y = top - 15 if top - 15 > 15 else top + 15
		if name == "Unknown":
			cv2.rectangle(combined_img, (left, top), (right, bottom), (255, 0, 0), 2)
		else:
			cv2.rectangle(combined_img, (left, top), (right, bottom), (0, 255, 0), 2)
			cv2.putText(combined_img, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

	return (ret, message, combined_img)
