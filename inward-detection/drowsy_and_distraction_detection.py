from scipy.spatial import distance
from imutils import face_utils
from threading import Thread
import datetime as dt
import pandas as pd
import numpy as np
import time
import dlib
import cv2


# initialize data array
columns_name = ['Average EAR', 'Left2right Ratio', 'Up2down Ratio']
data = pd.DataFrame(columns=columns_name)

# initialize time variables
time_now = dt.datetime.now()
drowsy_start = time_now
drowsy_end = time_now
distraction_start = time_now
distraction_end = time_now

# initiate dlib detector for facial landmarks prediction
detect = dlib.get_frontal_face_detector()
predict = dlib.shape_predictor("./inward-detection/shape-predictor-68-face-landmarks.dat")


def eye_aspect_ratio(eye):
    A = distance.euclidean(eye[1], eye[5])
    B = distance.euclidean(eye[2], eye[4])
    C = distance.euclidean(eye[0], eye[3])

    ear = (A + B) / (2.0 * C)

    return ear


def average_eye_aspect_ratio(left_eye, right_eye):
    left_ear = eye_aspect_ratio(left_eye)
    right_ear = eye_aspect_ratio(right_eye)

    avg_ear = round((left_ear + right_ear) / 2, 4)

    return avg_ear


def face_motion(shape):
    left = distance.euclidean(shape[0], shape[28])
    right = distance.euclidean(shape[16], shape[28])
    up = distance.euclidean(shape[30], shape[28])
    down = distance.euclidean(shape[30], shape[8])

    lrr = round(left / (left + right), 4)
    udr = round(up / (up + down), 4)

    return lrr, udr


# Python 3 has support for cool math symbols.
def weighted_img(img, initial_img, α=0.8, β=1., λ=0.):
    """
    `img` is the output of the hough_lines(), An image with lines drawn on it.
    Should be a blank image (all black) with lines drawn on it.
    
    `initial_img` should be the image before any processing.
    
    The result image is computed as follows:
    
    initial_img * α + img * β + λ
    NOTE: initial_img and img must be the same shape!
    """
    return cv2.addWeighted(initial_img, α, img, β, λ)


def process_image(frame, cap_time, combined_img, r):
    global data
    global drowsy_start
    global drowsy_end
    global distraction_start
    global distraction_end
    global detect
    global predict

    # initialize flag variables
    flag_no_detection = 0
    flag_drowsy = 0
    flag_distraction = 0

    # setting parameters level threshold
    thresh_avg_ear = 0.185
    thresh_lrr = [0.35, 0.65]
    thresh_udr = [0.1, 0.30]
    thresh_drowsy_duration = 1.5
    thresh_distraction_duration = 2.5
    
    # initialize message
    message = ''

    # extract landmarks index for facial features
    (lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
    (rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    subjects = detect(gray, 0)

    if len(subjects) >= 1:
        subject = subjects[0]

        for item in subjects:
            if item.area() > subject.area():
                subject = item

        # predict and locate facial coordinates
        shape = predict(gray, subject)
        shape = face_utils.shape_to_np(shape)

        # coordinates extraction by facial features
        left_eye = shape[lStart:lEnd]
        right_eye = shape[rStart:rEnd]
        
        # EAR computation
        avg_ear = round(average_eye_aspect_ratio(left_eye, right_eye), 4)
        lrr, udr = face_motion(shape)

        # drowsiness with ear threshold checking for sign and alert
        if avg_ear <= thresh_avg_ear:
            drowsy_end = cap_time

        elif avg_ear > thresh_avg_ear:
            drowsy_start = cap_time

        drowsy_duration = (drowsy_end - drowsy_start).total_seconds()

        if drowsy_duration >= thresh_drowsy_duration or (drowsy_duration > 0 and udr >= thresh_udr[1] and lrr > thresh_lrr[0] and lrr < thresh_lrr[1]):
            flag_drowsy = 1

        # distraction with left 2 right ratio and up 2 down ratio threshold checking for sign and alert
        if (lrr <= thresh_lrr[0] or lrr >= thresh_lrr[1] or udr >= thresh_udr[1]):
            distraction_end = cap_time

        elif (lrr > thresh_lrr[0] and lrr < thresh_lrr[1] and udr < thresh_udr[1]):
            distraction_start = cap_time

        distraction_duration = (distraction_end - distraction_start).total_seconds()

        if distraction_duration >= thresh_distraction_duration:
            flag_distraction = 1

        # print('Drowsy duration: ' + str(drowsy_duration) + '\nDistraction duration: ' + str(distraction_duration) + '\n')
        # print('Drowsy alert: ' + str(flag_drowsy) + '\nDistraction alert: ' + str(flag_distraction) + '\n')

    else:
        avg_ear = 0.000
        lrr = 0.000
        udr = 0.000

        flag_no_detection = 1

    if flag_no_detection:
        message = 'Warning: Face not detected!!'
    elif flag_drowsy:
        message = 'Warning: Drowsiness detected!!'
    elif flag_distraction:
        message = 'Warning: Distraction detected!!'

    ret = np.any([flag_no_detection, flag_drowsy, flag_distraction])    

    if not flag_no_detection:
        # marking 68 landmarks on frame for display
        for point in shape:
            cv2.circle(combined_img, (int(point[0] * r), int(point[1] * r)), 1, (0, 255, 0), -1)

        # connecting dots with line on points 0-28, 16-28, 28-30, 30-8 for lrr and udr visualisation
        cv2.line(combined_img, (int(shape[0][0] * r), int(shape[0][1] * r)), (int(shape[28][0] * r), int(shape[28][1] * r)), (0, 255, 0))
        cv2.line(combined_img, (int(shape[8][0] * r), int(shape[8][1] * r)), (int(shape[30][0] * r), int(shape[30][1] * r)), (0, 255, 0))
        cv2.line(combined_img, (int(shape[16][0] * r), int(shape[16][1] * r)), (int(shape[28][0] * r), int(shape[28][1] * r)), (0, 255, 0))
        cv2.line(combined_img, (int(shape[28][0] * r), int(shape[28][1] * r)), (int(shape[30][0] * r), int(shape[30][1] * r)), (0, 255, 0))

    data_row = pd.DataFrame([[avg_ear, lrr, udr]], columns=columns_name)

    size = len(data)

    if size >= 500:
        data = data.iloc[size - 499:].append(data_row, ignore_index=True)
    else:
        data = data.append(data_row, ignore_index=True)
    
    data.to_csv('data.csv', index=False)

    return (ret, message, combined_img)