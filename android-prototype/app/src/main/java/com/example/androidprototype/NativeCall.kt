package com.example.androidprototype

import android.content.res.AssetManager

class NativeCall {
    external fun loadShapePredictor(assetManager: AssetManager, fileName: String)

    external fun loadFacePredictor(modelPath: String)

    external fun inwardDetection(matAddrRgba: Long, height: Int, rotation: Int): Int

    companion object {
        init {
            // Cpp library file
            System.loadLibrary("native_jni")
        }
    }
}