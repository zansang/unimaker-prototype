package com.example.androidprototype

import android.Manifest.permission.CAMERA
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.OrientationEventListener
import android.view.SurfaceView
import android.view.WindowManager.LayoutParams.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.androidprototype.databinding.ActivityMainBinding
import org.opencv.android.CameraBridgeViewBase
import org.opencv.android.OpenCVLoader
import org.opencv.core.*
import org.opencv.core.Core.*
import java.io.File
import java.io.IOException
import java.util.*

// Permission vars:
private const val REQUEST_CODE_PERMISSIONS = 111
private val REQUIRED_PERMISSIONS = arrayOf(
    CAMERA,
//    WRITE_EXTERNAL_STORAGE,
//    READ_EXTERNAL_STORAGE,
//    RECORD_AUDIO,
//    ACCESS_FINE_LOCATION
)

class MainActivity : AppCompatActivity(), CameraBridgeViewBase.CvCameraViewListener2 {

    private lateinit var binding: ActivityMainBinding
    private lateinit var cameraBridgeViewBase: CameraBridgeViewBase

    lateinit var imageMat: Mat
    lateinit var grayMat: Mat

    // face library
    lateinit var faceDir: File
    lateinit var faceModel: File
    var screenRotation = 0 // screenOrientation
    var drowsyFlag = false
    var drowsyTime = Date()
    var distractFlag = false
    var distractTime = Date()

    companion object {
        val TAG = "MYLOG " + MainActivity::class.java.simpleName
        fun lgd(s: String) = Log.d(TAG, s)
        fun lge(s: String) = Log.e(TAG, s)
        fun lgi(s: String) = Log.i(TAG, s)

        fun shortMsg(context: Context, s: String) =
            Toast.makeText(context, s, Toast.LENGTH_SHORT).show()
        fun longMsg(context: Context, s: String) =
            Toast.makeText(context, s, Toast.LENGTH_LONG).show()


        // messages:
        private const val OPENCV_SUCCESSFUL = "OpenCV Loaded Successfully!"
        private const val OPENCV_FAIL = "Could not load OpenCV!!!"
        private const val OPENCV_PROBLEM = "There's a problem in OpenCV."
        private const val PERMISSION_NOT_GRANTED = "Permissions not granted by the user."

        // Face model
        private const val FACE_DIR = "facelib"
        private const val FACE_MODEL = "haarcascade_frontalface_alt2.xml"
        private const val byteSize = 4096 // buffer size
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.clearFlags(FLAG_FORCE_NOT_FULLSCREEN)
        window.setFlags(FLAG_FULLSCREEN, FLAG_FULLSCREEN)
        window.addFlags(FLAG_KEEP_SCREEN_ON)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        cameraBridgeViewBase = binding.cameraView
        cameraBridgeViewBase.keepScreenOn = true
        cameraBridgeViewBase.visibility = SurfaceView.VISIBLE
        cameraBridgeViewBase.setCameraIndex(CameraBridgeViewBase.CAMERA_ID_FRONT)
        cameraBridgeViewBase.setCvCameraViewListener(this)

        // Request camera permissions
        if (allPermissionsGranted()) {
            checkOpenCV(this)
        } else {
            ActivityCompat.requestPermissions(
                this,
                REQUIRED_PERMISSIONS,
                REQUEST_CODE_PERMISSIONS
            )
        }

        callFaceDetector()

        val mOrientationEventListener = object : OrientationEventListener(this) {
            override fun onOrientationChanged(orientation: Int) {
                // Monitors orientation values to determine the target rotation value
                when (orientation) {
                    in 45..134 -> {
                        binding.rotationTv.text = getString(R.string.n_90_degree)
                        screenRotation = 90
                    }
                    in 135..224 -> {
                        binding.rotationTv.text = getString(R.string.n_180_degree)
                        screenRotation = 180
                    }
                    in 225..314 -> {
                        binding.rotationTv.text = getString(R.string.n_270_degree)
                        screenRotation = 270
                    }
                    else -> {
                        binding.rotationTv.text = getString(R.string.n_0_degree)
                        screenRotation = 0
                    }
                }
            }
        }
        if (mOrientationEventListener.canDetectOrientation()) {
            mOrientationEventListener.enable();
        } else {
            mOrientationEventListener.disable();
        }
    }

    override fun onResume() {
        super.onResume()
        checkOpenCV(this)
        cameraBridgeViewBase.enableView()
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraBridgeViewBase.disableView()
        if (faceDir.exists()) faceDir.delete()
    }

    override fun onCameraViewStarted(width: Int, height: Int) {
        imageMat = Mat(width, height, CvType.CV_8UC4)
        grayMat = Mat(width, height, CvType.CV_8UC4)
    }

    override fun onCameraViewStopped() {
        imageMat.release()
        grayMat.release()
    }

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame?): Mat {
        var alertMessage = getString(R.string.no_alert)
        val currTime = Date()
        imageMat = inputFrame!!.rgba()
        when (NativeCall().inwardDetection(imageMat.nativeObjAddr, 480, screenRotation)) {
            0 -> alertMessage = getString(R.string.no_face_alert)
            1 -> {
                if (drowsyFlag) {
                    if (currTime.time - drowsyTime.time > 1500) {
                        alertMessage = getString(R.string.drowsy_alert)
                    }
                } else {
                    drowsyFlag = true
                    drowsyTime = currTime
                }
            }
            2 -> {
                if (distractFlag) {
                    if (currTime.time - distractTime.time > 2500) {
                        alertMessage = getString(R.string.distract_alert)
                    }
                } else {
                    distractFlag = true
                    distractTime = currTime
                }
            }
            else -> {
                drowsyFlag = false
                distractFlag = false
            }
        }
        binding.alertMessage.text = alertMessage
        return imageMat
    }

    /**
     * Process result from permission request dialog box, has the request
     * been granted? If yes, start Camera. Otherwise display a toast
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                checkOpenCV(this)
            } else {
                shortMsg(this, PERMISSION_NOT_GRANTED)
                finish()
            }
        }
    }

    /**
     * Check if all permission specified in the manifest have been granted
     */
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    /**
     * Check if OpenCV SDK is supported
     */
    private fun checkOpenCV(context: Context) {
        if (OpenCVLoader.initDebug()) {
//            shortMsg(context, OPENCV_SUCCESSFUL)
            lgd("OpenCV started...")
            cameraBridgeViewBase?.let {
                cameraBridgeViewBase.setCameraPermissionGranted()
                cameraBridgeViewBase.enableView()
                lgd("CameraView turned ON...")
            }
        } else {
            lge(OPENCV_PROBLEM)
        }
    }

    private fun callFaceDetector() {
        try {
            lgi(OPENCV_SUCCESSFUL)
            loadFaceLib()
            cameraBridgeViewBase.enableView()
        } catch (e: IOException) {
            lge(OPENCV_FAIL)
            shortMsg(this@MainActivity, OPENCV_FAIL)
            e.printStackTrace()
        }
    }

    private fun loadFaceLib() {
        try {
            // create a temp directory
            faceDir = getDir(FACE_DIR, Context.MODE_PRIVATE)
            // create a model file
            faceModel = File(faceDir, FACE_MODEL)

            NativeCall().loadFacePredictor(faceModel.absolutePath)
            NativeCall().loadShapePredictor(assets, "shape-predictor-68-face-landmarks.dat")

            faceDir.delete()
        } catch (e: IOException) {
            lge("Error loading cascade face model...$e")
        }
    }
}