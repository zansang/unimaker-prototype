//
// Created by Zan Sang Tan on 01/02/2021.
//

#include <dlib/opencv/cv_image.h>
#include "native_jni.h"

#define FACE_DOWNSAMPLE_RATIO 4

// Global Variables
CascadeClassifier faceDetector;
dlib::shape_predictor pose_model;

// RGB
Scalar RED = Scalar(255.0, 0.0, 0.0);
Scalar GREEN = Scalar(0.0, 255.0, 0.0);

bool color2Gray(Mat &img, Mat &gray);
double getRatio(Size size, int height);
void rotateMat(Mat &src, int rotation, bool invert = false);

float upDownRatio(full_object_detection& d);
float leftRightRatio(full_object_detection& d);
float averageEyeAspectRatio(full_object_detection& d);

void drawDot(Mat &src, float y, float x, Scalar color);
void drawBox(Mat &src, float y, float x, float h, float w, Scalar color);

struct membuf : std::streambuf {
    membuf(char* begin, char* end) {
        this->setg(begin, begin, end);
    }
};

extern "C"
JNIEXPORT void JNICALL
Java_com_example_androidprototype_NativeCall_loadShapePredictor(JNIEnv *env, jobject thiz, jobject assetManager, jstring fileName) {
    const char *file_name = env->GetStringUTFChars(fileName, nullptr);
    env->ReleaseStringUTFChars(fileName, file_name);

    // get AAssetManager
    AAssetManager *native_asset = AAssetManager_fromJava(env, assetManager);

    // open file
    AAsset *assetFile = AAssetManager_open(native_asset, file_name, AASSET_MODE_BUFFER);

    // get file length
    size_t file_length = static_cast<size_t>(AAsset_getLength(assetFile));
    char *model_buffer  = (char *) malloc(file_length);
    // read file data
    AAsset_read(assetFile, model_buffer, file_length);
    // the data has been copied to model_buffer, so, close it
    AAsset_close(assetFile);

    // char* to istream
    membuf mem_buf(model_buffer, model_buffer + file_length);
    std::istream  in(&mem_buf);

    // load share_predictor_68_face_landmarks.dat from memory
    dlib::deserialize(pose_model, in);

    // free malloc
    free(model_buffer);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_androidprototype_NativeCall_loadFacePredictor(JNIEnv *env, jobject thiz, jstring model_path) {
    // convert model_path to string
    const char *value = env->GetStringUTFChars(model_path, 0);
    String modelPath = (String) value;

    // load face detector
    faceDetector = CascadeClassifier();
    if (!faceDetector.load(modelPath)) {
        String error = "Error loading the model: " + modelPath;
        lge(error.c_str());
    } else {
        lgi("Model data is loaded.");
    }
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_example_androidprototype_NativeCall_inwardDetection(JNIEnv *env, jobject thiz, jlong mat_addr_rgba, jint height,
        jint rotation) {
    Mat& mRgb = *(Mat*) mat_addr_rgba;
    Mat mGr = Mat();

    if (!color2Gray(mRgb, mGr)) {
        lge("Grayscale conversion: failed!!!");
    }

    // downsize ratio of the grayscale image
    Size src = Size(mGr.size().width, mGr.size().height / FACE_DOWNSAMPLE_RATIO);
    double ratio = getRatio(src, height);

    // width and height of frame
    float scrW = (float)mRgb.size().width;
    float scrH = (float)mRgb.size().height;

    std::vector<Rect> faces;
    double scale = 1.0 + ratio;
    int neighbor = 3;

    // rotate for orientation adjustment
    rotateMat(mGr, rotation);
    dlib::cv_image<unsigned char> cgray(mGr);

    // face detection
    faceDetector.detectMultiScale(
            mGr,
            faces,
            scale,
            neighbor,
            0,
            Size(30, 30),
            Size((int) scrW, (int) scrH)
    );

    // face detected
    if (faces.size() >= 1) {
        Rect rect = faces[0];

        // filter closest face for landmark detection
        for (int i = 1; i < faces.size(); i++) {
            Rect temp = faces[1];
            if (temp.width * temp.height > rect.width * rect.height) {
                rect = temp;
            }
        }

        // detect landmarks
        dlib::rectangle iRect;
        iRect.set_left(rect.x);
        iRect.set_top(rect.y);
        iRect.set_right(rect.x + rect.width);
        iRect.set_bottom(rect.y + rect.height);
        full_object_detection shape = pose_model(cgray, iRect);

        float x = (float)rect.x;
        float y = (float)rect.y;
        float rw = (float)rect.width;
        float rh = (float)rect.height;
        float w = x + rw;
        float h = y + rh;

        // rotation for orientation adjustment
        rotateMat(mRgb, rotation);
        drawBox(mRgb, y, x, h, w, RED);
        drawDot(mRgb, y, x, GREEN);
        // rotation for orientation adjustment
        rotateMat(mRgb, rotation, true);

        // drowsy and distract detection
        float ear = averageEyeAspectRatio(shape);
        float lrr = leftRightRatio(shape);
        float udr = upDownRatio(shape);

        bool drowsy = (ear < 0.185);
        bool distract = (lrr < 0.35 | lrr > 0.65 | udr > 0.3);

        if (drowsy) {
            return 1;
        } else if (distract) {
            return 2;
        } else {
            return -1;
        }
    }

    return 0;
}

// Color conversion from RGBA to Gray
bool color2Gray(Mat &img, Mat &gray) {
    cvtColor(img, gray, COLOR_RGBA2GRAY);

    // compare rows and cols
    return img.rows == gray.rows && img.cols == gray.cols;
}

// Ratio between true size and grayscale size
double getRatio(Size src, int newSize) {
    short int w = static_cast<short>(src.width);
    short int h = static_cast<short>(src.height);
    short int heightMin = 320;

    if (newSize < heightMin) {
        lgd("Input size is too small! Set to 320px.");
    } else {
        heightMin = static_cast<short>(newSize);
    }

    float ratio;

    if (w > h) {
        if (w < heightMin) return 0.0;
        ratio = (float) heightMin / w;
    } else {
        if (h < heightMin) return 0.0;
        ratio = (float) heightMin / h;
    }

    return ratio;
}

void rotateMat(Mat &src, int rotation, bool invert) {
    int rotateCode = 0;
    if (rotation == 270) return ;

    switch (rotation) {
        case 0:
            rotateCode = invert ? ROTATE_90_CLOCKWISE : ROTATE_90_COUNTERCLOCKWISE;
            break;
        case 90:
            rotateCode = ROTATE_180;
            break;
        case 180:
            rotateCode = invert ? ROTATE_90_COUNTERCLOCKWISE : ROTATE_90_CLOCKWISE;
            break;
        default:
            string msg = "Error: wrong rotation data -- " + to_string(rotation);
            lge(msg.c_str());
            break;
    }

    rotate(src, src, rotateCode);
}

float euclideanDistance(full_object_detection& d, int p1, int p2) {
    cv::Point a = cv::Point(d.part(p1).x(), d.part(p1).y());
    cv::Point b = cv::Point(d.part(p2).x(), d.part(p2).y());
    float x = abs(a.x - b.x);
    float y = abs(a.y - b.y);
    return sqrt(pow(x, 2) + pow(y, 2));
}

float eyeAspectRatio(full_object_detection& d, int p) {
    float a = euclideanDistance(d, p + 1, p + 5);
    float b = euclideanDistance(d, p + 2, p + 4);
    float c = euclideanDistance(d, p + 0, p + 3);
    float ear = (a + b) / (2.0 * c);
    return ear;
}

float upDownRatio(full_object_detection& d) {
    float up = euclideanDistance(d, 30, 28);
    float down = euclideanDistance(d, 30, 8);
    float udr = round(up / (up + down) * 10000) / 10000;
    return udr;
}

float leftRightRatio(full_object_detection& d) {
    float left = euclideanDistance(d, 0, 28);
    float right = euclideanDistance(d, 16, 28);
    float lrr = round(left / (left + right) * 10000) / 10000;
    return lrr;
}

float averageEyeAspectRatio(full_object_detection& d) {
    float left_ear = eyeAspectRatio(d, 36);
    float right_ear = eyeAspectRatio(d, 42);
    float avg_ear = round((left_ear + right_ear) / 2 * 10000) / 10000;
    return avg_ear;
}

void drawDot(Mat &src, float y, float x, Scalar color) {
    cv::circle(src, Point(x, y), 4, color, -1, 8);
}

void drawBox(Mat &src, float y, float x, float h, float w, Scalar color) {
    cv::rectangle(src, Point(x, y), Point(w, h), color);
}
