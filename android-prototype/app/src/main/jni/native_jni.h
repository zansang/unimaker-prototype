//
// Created by Zan Sang Tan on 01/02/2021.
//

#ifndef ANDROID_PROTOTYPE_NATIVE_JNI_H
#define ANDROID_PROTOTYPE_NATIVE_JNI_H

#include <jni.h>
#include <string>
#include <stdio.h>

#include <opencv2/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/objdetect.hpp>

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <iostream>

#include <android/asset_manager_jni.h>

using namespace std;
using namespace cv;
using namespace dlib;

#include <android/log.h>

// Logcat
#define TAG "NativeJni"
void lgi(const string s);
// Android Logcat: debug
void lgd(const string s);
// Android Logcat: error
void lge(const string s);

class native_jni {

};

// Android Logcat: info
void lgi(const string s) {
    __android_log_print(ANDROID_LOG_INFO, TAG, "%s", s.c_str());
}
// Android Logcat: debug
void lgd(const string s) {
    __android_log_print(ANDROID_LOG_DEBUG, TAG, "%s", s.c_str());
}
// Android Logcat: error
void lge(const string s) {
    __android_log_print(ANDROID_LOG_ERROR, TAG, "%s", s.c_str());
}

#endif //ANDROID_PROTOTYPE_NATIVE_JNI_H
